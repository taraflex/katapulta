server_tokens off;
autoindex off;

access_log /var/log/nginx/access.log;
error_log /var/log/nginx/error.log;

map $http_upgrade $connection_upgrade {
  default upgrade;
  ""      close;
}

{{#SSL_KEY}}
server {
    listen 80;
    listen [::]:80 ipv6only=on;
    server_name www.{{HOSTNAME}} {{HOSTNAME}};
    return 301 https://{{HOSTNAME}}$request_uri;
}
{{/SSL_KEY}}

server {
    {{#SSL_KEY}}
    listen 443 ssl default_server;
    listen [::]:443 ssl default_server ipv6only=on;
    {{SSL_CERT}}
    {{SSL_KEY}}
    ssl_protocols TLSv1.2;
    {{/SSL_KEY}}
    {{^SSL_KEY}}
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;
    {{/SSL_KEY}}
  
    root /home/katapulta;
    server_name {{HOSTNAME}};

    index index.php;
    
    if ($http_user_agent = ""){ 
        return 444;
    }

    if ($request_method !~ ^(GET|POST|PATCH|PUT|OPTIONS|DELETE)$ ) {
        return 444;
    }

    if ($http_user_agent ~* java|curl|wget|winhttp|HTTrack|clshttp|archiver|loader|email|harvest|extract|grab|miner|libwww-perl|curl|wget|python|nikto|scan|http-client){
        return 444;
    }

    location / {
        # Let OctoberCMS handle everything by default.
        # The path not resolved by OctoberCMS router will return OctoberCMS's 404 page.
        # Everything that does not match with the whitelist below will fall into this.
        rewrite ^/.*$ /index.php last;
    }

    # Pass the PHP scripts to FastCGI server
    location ~ ^/index.php {
      {{#SSL_KEY}}
      add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
      {{/SSL_KEY}}
      add_header X-Frame-Options "DENY" always;
      add_header X-Content-Type-Options "nosniff" always;
      add_header X-Permitted-Cross-Domain-Policies "none" always;
      add_header X-XSS-Protection "1; mode=block" always;

      include /etc/nginx/fastcgi.conf;
      fastcgi_pass unix:/run/php/php7.3-fpm.sock;
    }

# Whitelist
## Let October handle if static file not exists
location ~ ^/favicon\.ico { try_files $uri /index.php; }
location ~ ^/sitemap\.xml { try_files $uri /index.php; }
location ~ ^/robots\.txt { try_files $uri /index.php; }
location ~ ^/humans\.txt { try_files $uri /index.php; }

## Let nginx return 404 if static file not exists
location ~ ^/storage/app/uploads/public { try_files $uri 404; }
location ~ ^/storage/app/media { try_files $uri 404; }
location ~ ^/storage/temp/public { try_files $uri 404; }

location ~ ^/modules/.*/assets { try_files $uri 404; }
location ~ ^/modules/.*/resources { try_files $uri 404; }
location ~ ^/modules/.*/behaviors/.*/assets { try_files $uri 404; }
location ~ ^/modules/.*/behaviors/.*/resources { try_files $uri 404; }
location ~ ^/modules/.*/widgets/.*/assets { try_files $uri 404; }
location ~ ^/modules/.*/widgets/.*/resources { try_files $uri 404; }
location ~ ^/modules/.*/formwidgets/.*/assets { try_files $uri 404; }
location ~ ^/modules/.*/formwidgets/.*/resources { try_files $uri 404; }
location ~ ^/modules/.*/reportwidgets/.*/assets { try_files $uri 404; }
location ~ ^/modules/.*/reportwidgets/.*/resources { try_files $uri 404; }

location ~ ^/plugins/.*/.*/assets { try_files $uri 404; }
location ~ ^/plugins/.*/.*/resources { try_files $uri 404; }
location ~ ^/plugins/.*/.*/behaviors/.*/assets { try_files $uri 404; }
location ~ ^/plugins/.*/.*/behaviors/.*/resources { try_files $uri 404; }
location ~ ^/plugins/.*/.*/reportwidgets/.*/assets { try_files $uri 404; }
location ~ ^/plugins/.*/.*/reportwidgets/.*/resources { try_files $uri 404; }
location ~ ^/plugins/.*/.*/formwidgets/.*/assets { try_files $uri 404; }
location ~ ^/plugins/.*/.*/formwidgets/.*/resources { try_files $uri 404; }
location ~ ^/plugins/.*/.*/widgets/.*/assets { try_files $uri 404; }
location ~ ^/plugins/.*/.*/widgets/.*/resources { try_files $uri 404; }

location ~ ^/themes/.*/assets { try_files $uri 404; }
location ~ ^/themes/.*/resources { try_files $uri 404; }

    # Gzip compression
    gzip on;
    gzip_comp_level 5;
    gzip_min_length 256;
    gzip_proxied any;
    gzip_vary on;
    gzip_types
        application/atom+xml
        application/javascript
        application/json
        application/ld+json
        application/manifest+json
        application/rss+xml
        application/vnd.geo+json
        application/vnd.ms-fontobject
        application/x-font-ttf
        application/x-web-app-manifest+json
        application/xhtml+xml
        application/xml
        font/opentype
        image/bmp
        image/svg+xml
        image/x-icon
        text/cache-manifest
        text/css
        text/plain
        text/vcard
        text/vnd.rim.location.xloc
        text/vtt
        text/x-component
        text/x-cross-domain-policy;
}