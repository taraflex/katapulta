function save_options() {
    chrome.storage.sync.set({
        likeVk: document.getElementById('like_vk').checked,
        likeFb: document.getElementById('like_fb').checked,
        likeYtb: document.getElementById('like_ytb').checked,
        likeInst: document.getElementById('like_inst').checked,
    }, function () {
        let status = document.getElementById('status');
        status.textContent = 'Настройки сохранены';
        chrome.extension.getBackgroundPage().window.location.reload();
        setTimeout(function () {
            status.textContent = '';
        }, 1000);
    });
}

function restore_options() {
    chrome.storage.sync.get({
        likeVk: false,
        likeFb: false,
        likeYtb: false,
        likeInst: false
    }, function (items) {
        document.getElementById('like_vk').checked = items.likeVk;
        document.getElementById('like_fb').checked = items.likeFb;
        document.getElementById('like_ytb').checked = items.likeYtb;
        document.getElementById('like_inst').checked = items.likeInst;
    });
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);