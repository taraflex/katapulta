const HOSTNAME = new URL(chrome.runtime.getManifest().permissions[0]).host;

const queueHtml = chrome.runtime.getURL("queue.html");
const helloHtml = chrome.runtime.getURL("hello.html");

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

let myLog = function (msg) {
    if (typeof console !== 'undefined' && typeof console.debug !== 'undefined') {
        console.debug(msg);
    }
};

let rmFromServer = function (itemID) {
    let request = new XMLHttpRequest();
    request.open('GET', 'http://' + HOSTNAME + '/task-done-' + itemID + '?vkid=' + myVkID, false);
    request.send(null);
};

/**
 * alarms
 */

chrome.alarms.onAlarm.addListener(function (alarm) {
    for (let i in [1, 2, 3, 4, 5]) {
        alarm.name = alarm.name.replace('scroll-' + i, 'scroll');
    }

    myLog(alarm.name);
    if (alarm.name === 'scroll-tab') {
        try {
            chrome.tabs.executeScript(myTabID, {
                //language=JavaScript
                code: "try {\n    window.scrollTo(0, Math.floor(Math.random() * (1000 - 100 + 1)) + 100)\n} catch (err) {\n    /*console.log(err);*/\n}"
            });
        } catch (e) {
            if (chrome.runtime.lastError) {
                reCreateMyTabIfNotExists();
            }
        }
    } else /*if (alarm.name.indexOf('rm-from-server-') === 0) {
        let itemID = alarm.name.replace('rm-from-server-', '');
        try {
            rmFromServer(itemID);
        } catch (e) {

        }
    } else*/ if (alarm.name === 'make-vk-like-tab') {
            try {
                chrome.tabs.executeScript(myTabID, {
                    //language=JavaScript
                    code: "try {\n\n    if (document.getElementById(\'wl_post_actions_wrap\') !== null && document.getElementById(\'wl_post_actions_wrap\').getElementsByClassName(\'like_btn\')[0].className.indexOf(\'active\') === -1) {\n        document.getElementById(\'wl_post_actions_wrap\').getElementsByClassName(\'like_btn\')[0].click();\n    } else if (document.getElementById(\'wk_content\') !== null && document.getElementById(\'wk_content\').getElementsByClassName(\'post_like\').length > 0 && document.getElementById(\'wk_content\').getElementsByClassName(\'post_like\')[0].className.indexOf(\'my_like\') === -1) {\n        document.getElementById(\'wk_content\').getElementsByClassName(\'post_like\')[0].click();\n    } else if (document.getElementsByClassName(\'post_full_like\').length > 0 && document.getElementsByClassName(\'post_full_like\')[0].getElementsByClassName(\'post_like\').length > 0 && document.getElementsByClassName(\'post_full_like\')[0].getElementsByClassName(\'post_like\')[0].className.indexOf(\'my_like\') === -1) {\n        document.getElementsByClassName(\'post_full_like\')[0].getElementsByClassName(\'post_like\')[0].click();\n    } else if (document.getElementById(\'wl_post\') !== null && (typeof document.getElementById(\'wl_post\').getElementsByClassName(\'post_like\')[0] != \'undefined\') && document.getElementById(\'wl_post\').getElementsByClassName(\'post_like\')[0].className.indexOf(\'my_like\') === -1) {\n        document.getElementById(\'wl_post\').getElementsByClassName(\'post_like\')[0].click();\n    } else if (document.getElementById(\'pv_like\') !== null && document.getElementById(\'pv_like\').className.indexOf(\'pv_liked\') === -1) {\n        document.getElementById(\'pv_like\').click()\n    } else if (document.getElementsByClassName(\'wi_buttons\').length > 0 && document.getElementsByClassName(\'wi_buttons\')[0].getElementsByClassName(\'item_like\')[0].className.indexOf(\'item_sel\') == -1) {\n        document.getElementsByClassName(\'wi_buttons\')[0].getElementsByClassName(\'item_like\')[0].click()\n    } else if (document.getElementsByClassName(\'market_like\').length > 0 && document.getElementsByClassName(\'market_like\')[0].className.indexOf(\'my_like\') === -1) {\n        document.getElementsByClassName(\'market_like\')[0].click();\n    } else if (document.getElementsByClassName(\'like_btn\').length > 0 && document.getElementsByClassName(\'like_btn\')[0].className.indexOf(\'active\') === -1) {\n        document.getElementsByClassName(\'like_btn\')[0].click();\n    }\n} catch (err) {\n    /*console.log(err);*/\n}"
                }, function () {
                    setTimeout(function () {
                        setBadge('wait');
                        chrome.tabs.update(myTabID, { url: queueHtml });
                        working = false;
                    }, 4000);
                });
            } catch (e) {
                if (chrome.runtime.lastError) {
                    reCreateMyTabIfNotExists();
                }
            }
        } else if (alarm.name === 'make-fb-like-tab') {
            try {
                chrome.tabs.executeScript(myTabID, {
                    //language=JavaScript
                    code: "try {\n    if (document.getElementsByClassName(\'like-button-renderer-like-button-unclicked\').length > 0) {\n        document.getElementsByClassName(\'like-button-renderer-like-button-unclicked\')[0].click();\n    } else if (document.getElementsByClassName(\'UFILikeLink\').length > 0 && document.getElementsByClassName(\'UFILikeLink\')[0].className.indexOf(\'UFILinkBright\') === -1) {\n        document.getElementsByClassName(\'UFILikeLink\')[0].click();\n    } else if (document.querySelectorAll(\'.uiScrollableAreaBody a[data-testid=\"UFI2ReactionLink\"]\').length > 0) {\n        document.querySelectorAll(\'.uiScrollableAreaBody a[data-testid=\"UFI2ReactionLink\"]\')[0].click()\n    } else if (document.querySelectorAll(\'a[data-testid=\"UFI2ReactionLink\"]\').length == 1) {\n        document.querySelectorAll(\'a[data-testid=\"UFI2ReactionLink\"]\')[0].click()\n    } else if (document.querySelectorAll(\'[data-testid=\"UFI2ReactionLink/actionLink\"] a[data-testid=\"UFI2ReactionLink\"]\').length == 1) {\n        document.querySelectorAll(\'[data-testid=\"UFI2ReactionLink/actionLink\"] a[data-testid=\"UFI2ReactionLink\"]\')[0].click()\n    }\n} catch\n    (err) {\n    /*console.log(err);*/\n}"
                }, function () {
                    setTimeout(function () {
                        setBadge('wait');
                        chrome.tabs.update(myTabID, { url: queueHtml });
                        working = false;
                    }, 4000);
                });
            } catch (e) {
                if (chrome.runtime.lastError) {
                    reCreateMyTabIfNotExists();
                }
            }
        } else if (alarm.name === 'make-ytb-like-tab') {
            try {
                chrome.tabs.executeScript(myTabID, {
                    //language=JavaScript
                    code: "try {\n    if (document.getElementById(\'menu-container\') !== null && document.getElementById(\'menu-container\').getElementsByClassName(\'force-icon-button\').length > 0) {\n        document.getElementById(\'menu-container\').getElementsByClassName(\'force-icon-button\')[0].click()\n    } else if (document.getElementById(\'watch8-sentiment-actions\') !== null) {\n        document.getElementById(\'watch8-sentiment-actions\').getElementsByClassName(\'like-button-renderer-like-button\')[0].click()\n    } else if (document.getElementById(\'info\') !== null && document.getElementById(\'info\').getElementsByClassName(\'force-icon-button\').length > 0 && document.getElementById(\'info\').getElementsByClassName(\'force-icon-button\')[0].className.indexOf(\'style-default-active\') === -1) {\n        document.getElementById(\'info\').getElementsByClassName(\'force-icon-button\')[0].click();\n    }\n} catch (err) {\n    /*console.log(err);*/\n}\n"
                }, function () {
                    setTimeout(function () {
                        setBadge('wait');
                        chrome.tabs.update(myTabID, { url: queueHtml });
                        working = false;
                    }, 9000);
                });
            } catch (e) {
                if (chrome.runtime.lastError) {
                    reCreateMyTabIfNotExists();
                }
            }
        } else if (alarm.name === 'make-inst-like-tab') {
            try {
                chrome.tabs.executeScript(myTabID, {
                    //language=JavaScript
                    code: "try {\n    if (document.querySelectorAll(\'button>.glyphsSpriteHeart__outline__24__grey_9\').length > 0) {\n        document.querySelectorAll(\'button>.glyphsSpriteHeart__outline__24__grey_9\')[0].click();\n    }\n} catch (err) {\n    /*console.log(err);*/\n}"
                }, function () {
                    setTimeout(function () {
                        setBadge('wait');
                        chrome.tabs.update(myTabID, { url: queueHtml });
                        working = false;
                    }, 4000);
                });
            } catch (e) {
                if (chrome.runtime.lastError) {
                    reCreateMyTabIfNotExists();
                }
            }
        }

});


/**
 *
 */
let likeVk = function (url, itemID) {
    myLog('vk:start ' + url + ', item ID=' + itemID);
    chrome.tabs.update(myTabID, { 'url': url, pinned: true, muted: true, autoDiscardable: false }, function (Tab) {
        for (let i = 1; i < 4; i++) {
            chrome.alarms.create('scroll-' + i + '-tab', { delayInMinutes: getRandomInt(i + 3, 19) / 60 });
        }
        chrome.alarms.create('make-vk-like-tab', { delayInMinutes: getRandomInt(20, 40) / 60 });

        rmFromServer(itemID);
        // chrome.alarms.create('rm-from-server-' + itemID, {delayInMinutes: 1});

    });
};

let likeFb = function (url, itemID) {
    myLog('fb:start ' + url + ', item ID=' + itemID);
    chrome.tabs.update(myTabID, { 'url': url, pinned: true, muted: true, autoDiscardable: false }, function (Tab) {
        for (let i = 1; i < 4; i++) {
            chrome.alarms.create('scroll-' + i + '-tab', { delayInMinutes: getRandomInt(i + 3, 19) / 60 });
        }
        chrome.alarms.create('make-fb-like-tab', { delayInMinutes: getRandomInt(20, 40) / 60 });

        rmFromServer(itemID);
        // chrome.alarms.create('rm-from-server-' + itemID, {delayInMinutes: 1});
    });
};

let likeYtb = function (url, itemID) {
    myLog('ytb:start ' + url + ', item ID=' + itemID);

    chrome.tabs.update(myTabID, { 'url': url, pinned: true, muted: true, autoDiscardable: false }, function (Tab) {
        /*
                chrome.tabs.query(myTabID, {active: true}, function (tabs) {
                    if (typeof tabs !== 'undefined' && typeof tabs[0] !== 'undefined' && tabs[0].id !== 'undefined') {
                        lastActiveTabID = tabs[0].id;
                        chrome.tabs.update(myTabID, {active: true}, function (Tab) {
                            chrome.tabs.update(tabs[0].id, {active: true});
                        });
                    }
                });
        */

        for (let i = 1; i < 5; i++) {
            chrome.alarms.create('scroll-' + i + '-tab', { delayInMinutes: getRandomInt(i + 3, 60) / 60 });
        }
        chrome.alarms.create('make-ytb-like-tab', { delayInMinutes: getRandomInt(300, 600) / 60 });

        rmFromServer(itemID);
        // chrome.alarms.create('rm-from-server-' + itemID, {delayInMinutes: 1});

    });
};

let likeInst = function (url, itemID) {
    myLog('inst:start ' + url + ', item ID=' + itemID);
    chrome.tabs.update(myTabID, { 'url': url, pinned: true, muted: true, autoDiscardable: false }, function (Tab) {
        for (let i = 1; i < 4; i++) {
            chrome.alarms.create('scroll-' + i + '-tab', { delayInMinutes: getRandomInt(i + 3, 19) / 60 });
        }
        chrome.alarms.create('make-inst-like-tab', { delayInMinutes: getRandomInt(20, 40) / 60 });

        rmFromServer(itemID);
        // chrome.alarms.create('rm-from-server-' + itemID, {delayInMinutes: 1});
    });
};

let started = false;
let ba = chrome.browserAction;
let working = false;
let queuedItems = [];
let myVkID = null;
let myTabID = null;

let ticksBySocType = [];

let options = {};

let worker = null;

let URLexpression = /(https?:[^\s]+)/;

// let unsubscribedCnt = 0;

let tickedWorkingSeconds = 0;

// let realQueueLength = 0;
let socialTypes = [];


setBadge = function (message = '') {
    if (typeof message !== 'undefined' && message.length > 0) {
        ba.setBadgeBackgroundColor({ color: [255, 74, 74, 128] });
        ba.setBadgeText({ text: message });
    } else if (queuedItems.length >= 0) {
        ba.setBadgeBackgroundColor({ color: [255, 74, 74, 128] });
        ba.setBadgeText({ text: '' + queuedItems.length });
    }
};


getMyVKID = function () {
    myLog('get vk id');
    reCreateMyTabIfNotExists();
    chrome.tabs.update(myTabID, { url: 'https://vk.com/settings' }, function (Tab) {
        setTimeout(function () {
            reCreateMyTabIfNotExists();
            try {
                chrome.tabs.executeScript(Tab.id, {
                    //language=JavaScript
                    code: "parseInt(document.getElementsByClassName(\'settings_row_hint\')[0].getElementsByTagName(\'b\')[0].innerHTML)"
                }, function (vkID) {
                    myVkID = parseInt(vkID);
                    if (!isNaN(myVkID) && myVkID !== null && myVkID > 0) {
                        chrome.storage.sync.set({ 'myVkID': myVkID });
                    }
                    myLog('vk id: ' + myVkID);
                    working = false;
                }
                );
            } catch (err) {
                myLog(err);
                setBadge(':(');
                working = false;
            }
        }, 5000);
    });
};

doOnTimeout = function () {
    myLog('tick and working = ' + working);
    if (working) {
        return;
    }

    working = true;
    tickedWorkingSeconds++;

    if (isNaN(myVkID) || myVkID === null) {
        if (tickedWorkingSeconds > options.tickTimerGetVkID) {
            setBadge('VkID');
            getMyVKID();
            options.tickTimerGetVkID = 300;
        } else {
            setBadge('');
            working = false;
        }
        return;
    }

    if (tickedWorkingSeconds > options.tickTimerDoNextLike) {
        if (queuedItems.length === 0) {
            let request = new XMLHttpRequest();
            let ddd = new Date();
            request.open('GET', 'http://' + HOSTNAME + '/get-tasks-' + myVkID + '?t=' + ddd.getTime() + '&socTypes=' + socialTypes.join(','), false);
            request.onload = function (e) {
                if (request.status === 200) {
                    let resp = JSON.parse(request.responseText);
                    if (typeof resp === 'object' && resp !== null) {
                        for (let i in resp) {
                            let urlParsed = URLexpression.exec(resp[i].url);
                            if (urlParsed !== null && typeof urlParsed[0] !== 'undefined') {
                                resp[i].url = urlParsed[0];
                                queuedItems.push(resp[i]);
                            }
                        }
                    }
                    if (options.tickTimerDoNextLike !== 30) {
                        options.tickTimerDoNextLike = 30;
                    }
                    tickedWorkingSeconds = 0;
                    // setBadge();
                } else {
                    queuedItems = [];
                    options.tickTimerDoNextLike = 15;
                    setBadge('...');
                }
                myLog(queuedItems);
            };
            request.send(null);
        }

        if (queuedItems.length > 0) {
            try {
                reCreateMyTabIfNotExists();

                let curItem = queuedItems.shift();
                if (typeof ticksBySocType[curItem.socialType] === 'undefined' || ticksBySocType[curItem.socialType] === 0) {
                    switch (curItem.socialType) {
                        case 0:
                            setBadge('vk');
                            likeVk(curItem.url, curItem.id);
                            break;
                        case 1:
                            setBadge('fb');
                            likeFb(curItem.url, curItem.id);
                            break;
                        case 2:
                            setBadge('ytb');
                            likeYtb(curItem.url, curItem.id);
                            break;
                        case 3:
                            setBadge('inst');
                            likeInst(curItem.url, curItem.id);
                            break;
                        default:
                            working = false;
                    }
                    ticksBySocType[curItem.socialType] = 2;//!!!
                } else {
                    setBadge('wait');
                    ticksBySocType[curItem.socialType]--;
                    queuedItems.push(curItem);
                    working = false;
                }
            } catch (err) {
                myLog(error);
                working = false;
            }
            tickedWorkingSeconds = 0;
        } else {
            working = false;
            tickedWorkingSeconds = 0;
        }
    } else {
        setBadge('wait');
        working = false;
    }
};

let lastActiveTabID = false;
reCreateMyTabIfNotExists = function () {
    myLog('check my tab ' + myTabID);
    chrome.tabs.get(myTabID > 0 ? myTabID : 0, function (Tab) {
        if (chrome.runtime.lastError) {
            chrome.tabs.query({ active: true }, function (tabs) {
                if (typeof tabs[0] !== 'undefined' && tabs[0].id !== 'undefined') {
                    lastActiveTabID = tabs[0].id;
                }
                chrome.tabs.create({ url: helloHtml, active: true, pinned: true,/* discarded: false, autoDiscardable: false, mutedInfo: { muted: true }*/ }, function (Tab) {
                    myTabID = Tab.id;
                    chrome.storage.sync.set({ oldTabID: myTabID });
                    myLog('my tab id now ' + myTabID);
                    if (lastActiveTabID !== false) {
                        chrome.tabs.update(lastActiveTabID, { active: true });
                    }
                });
            });
        } else {

        }
    });
};

chrome.storage.sync.get({
    likeVk: false,
    likeFb: false,
    likeYtb: false,
    likeInst: false,
    tickTimerDoNextLike: 5,//!!!
    tickTimerGetVkID: 3, // changed after first query
    sleepState: true,
    oldTabID: null,
    myVkID: null
}, function (items) {
    if (started !== true) {
        started = true;
        options = items;
        myLog('init options');
        myLog(options);

        if (
            !options.likeVk
            && !options.likeFb
            && !options.likeYtb
            && !options.likeInst
        ) {
            setBadge('no');
            myLog('no social active services in options');
        } else {
            if (options.likeVk !== 'undefined' && options.likeVk === true) {
                socialTypes.push(0);
            }

            if (options.likeFb !== 'undefined' && options.likeFb === true) {
                socialTypes.push(1);
            }

            if (options.likeYtb !== 'undefined' && options.likeYtb === true) {
                socialTypes.push(2);
            }

            if (options.likeInst !== 'undefined' && options.likeInst === true) {
                socialTypes.push(3);
            }


            myVkID = options.myVkID;

            if (options.sleepState === true) {
                chrome.browserAction.setIcon({ path: "icon_off.png" });
                setBadge('off');
            } else {
                chrome.browserAction.setIcon({ path: "icon.png" });

                if (myTabID !== null) {
                    chrome.tabs.get(myTabID, function (Tab) {
                        if (chrome.runtime.lastError) {

                        } else {
                            chrome.tabs.remove(Tab.id);
                        }
                    });
                }
                myTabID = options.oldTabID;
                reCreateMyTabIfNotExists();

                setBadge(':)');
                if (worker !== null) {
                    chrome.tabs.update(myTabID, { url: queueHtml });
                    myLog('kill old worker');
                    chrome.alarms.clearAll();
                    clearInterval(worker);
                }

                tickedWorkingSeconds = 0;
                working = false;
                worker = setInterval(doOnTimeout, 1000);
            }
        }
    }
});

chrome.runtime.onStartup.addListener(function () {
    // chrome.extension.getBackgroundPage().window.location.reload();
});

chrome.runtime.onInstalled.addListener(function (details) {
    if (details.reason == "install") {
        myLog("This is a first install!");
        chrome.runtime.openOptionsPage();
    } else if (details.reason == "update") {
        var thisVersion = chrome.runtime.getManifest().version;
        // alert('Катапульта: установлено обновление, перезагрузите браузер, когда сможете.')
        myLog("Updated from " + details.previousVersion + " to " + thisVersion + "!");
    }
});

chrome.browserAction.onClicked.addListener(function (tab) {
    chrome.storage.sync.get({
        sleepState: true,
        oldTabID: null
    }, function (items) {
        myLog("Click: sleepState = " + (items.sleepState === true ? 'true' : 'false'));
        if (items.sleepState === true) {
            chrome.storage.sync.set({ sleepState: false });
        } else {
            if (myTabID !== null) {
                chrome.tabs.get(myTabID, function (Tab) {
                    if (chrome.runtime.lastError) {

                    } else {
                        chrome.tabs.remove(Tab.id);
                    }
                });
            }
            if (items.oldTabID !== null) {
                chrome.tabs.get(items.oldTabID, function (Tab) {
                    if (chrome.runtime.lastError) {

                    } else {
                        chrome.tabs.remove(Tab.id);
                    }
                });
            }
            chrome.storage.sync.set({ sleepState: true, oldTabID: null });
            started = false;
            if (worker !== null) {
                myLog('kill old worker');
                chrome.alarms.clearAll();
                working = false;
                clearInterval(worker);
                worker = null;
            }
        }
        chrome.extension.getBackgroundPage().window.location.reload();
    });
});