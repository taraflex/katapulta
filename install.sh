apt-get update -y 
apt-get upgrade --yes --force-yes 
apt-get remove apache2 -y 
wget -qO- https://deb.nodesource.com/setup_12.x | bash
apt-get install git lsof man htop ufw socat nginx postgresql postgresql-contrib curl php-cli php-fpm php-curl php-pgsql php-zip php-dom php-gd php-mbstring unzip nodejs -y

npm install -g crx3

mkdir -p /home/katapulta
cd /home/katapulta

if [ -z $(which mo) ]; then
    curl -sSL https://git.io/get-mo -o /usr/local/bin/mo
    chmod +x /usr/local/bin/mo
fi

if [ -z $(which composer) ]; then
    curl -sSL https://getcomposer.org/installer -o composer-setup.php
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer
    rm -f ./composer-setup.php
fi

source ~/.bashrc

nginx_gen () {
    export HOSTNAME=$(cat /etc/hosts | grep $(hostname) | tr '\t ' '\n' | sort -r | head -1)
    echo "include /home/katapulta/nginx.conf;" > /etc/nginx/sites-available/$HOSTNAME
    ln -sf /etc/nginx/sites-available/$HOSTNAME /etc/nginx/sites-enabled/
    rm -f /etc/nginx/sites-enabled/default

    if test -f /etc/nginx/ssl/cert.pem && test -f /etc/nginx/ssl/key.pem; then
        export SSL_CERT="ssl_certificate /etc/nginx/ssl/cert.pem;" 
        export SSL_KEY="ssl_certificate_key /etc/nginx/ssl/key.pem;" 
    else
        export SSL_CERT=""
        export SSL_KEY=""
    fi
    mo /home/katapulta/nginx.mustache.conf > /home/katapulta/nginx.conf
}

nginx_gen

systemctl restart postgresql
systemctl restart php7.3-fpm
systemctl restart nginx

systemctl enable postgresql
systemctl enable php7.3-fpm
systemctl enable nginx

if id vagrant >/dev/null 2>&1; then
    echo '';
else
    mkdir -p /etc/nginx/ssl
    chown -R root:root /etc/nginx/ssl/
    chmod -R 0640 /etc/nginx/ssl/ #read -og / write -o
    curl -sSL https://get.acme.sh | sh
    /root/.acme.sh/acme.sh --issue --nginx -d $HOSTNAME 
    /root/.acme.sh/acme.sh --install-cert --force -d $HOSTNAME --key-file /etc/nginx/ssl/key.pem --fullchain-file /etc/nginx/ssl/cert.pem --reloadcmd "systemctl reload nginx"
    nginx_gen
fi

chmod +x ./genext.sh
./genext.sh

if id vagrant >/dev/null 2>&1; then
    export DBPASSWORD="vagrant"
    export DBUSER="vagrant"
else 
    ufw default deny incoming
    ufw allow ssh
    ufw allow http
    ufw allow https
    ufw logging off
    ufw --force enable

    export DBPASSWORD=$(LC_CTYPE=C tr -d -c '[:alnum:]' </dev/urandom | head -c 72)
    export DBUSER=$(LC_CTYPE=C tr -d -c '[:alnum:]' </dev/urandom | head -c 20)
fi
sudo -u postgres psql -U postgres -c "DROP DATABASE IF EXISTS katapulta"
sudo -u postgres psql -U postgres -c "CREATE DATABASE katapulta"
sudo -u postgres psql -U postgres -c "create user $DBUSER with encrypted password '$DBPASSWORD'"
sudo -u postgres psql -U postgres -c "grant all privileges on database katapulta to $DBUSER"

echo 'Forum auth data:'
echo -e "DB_USER $DBUSER\nDB_PASSWORD $DBPASSWORD" | column -t -s ' '
curl -s https://octobercms.com/api/installer | php

# php artisan october:install
# sudo chown -R www-data:www-data ./vendor
# sudo chown -R www-data:www-data ./storage
# php artisan plugin:refresh taraflex.katapulta