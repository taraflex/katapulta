mkdir -p ./extension_build
test ! -s ./extension_build/extension.pem && openssl genrsa 2048 | openssl pkcs8 -topk8 -nocrypt -out ./extension_build/extension.pem

export HOSTNAME=$(cat /etc/hosts | grep $(hostname) | tr '\t ' '\n' | sort -r | head -1)
mo ./manifest.mustache.json > ./extension/manifest.json
rm -f ./extension_build/extension.zip
rm -f ./extension_build/extension.crx
crx3 -p extension_build/extension.pem -z extension_build/extension.zip -o extension_build/extension.crx ./extension