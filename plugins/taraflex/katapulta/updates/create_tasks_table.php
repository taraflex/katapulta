<?php 

namespace Taraflex\Katapulta\Updates;

use Illuminate\Support\Facades\DB;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    public function up()
    {
        Schema::create('taraflex_katapulta_tasks', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('url');
            $table->unsignedSmallInteger('socialType')->index();
            $table->unsignedInteger('processed')->default(0);
        });
        DB::statement("ALTER TABLE taraflex_katapulta_tasks ADD COLUMN IF NOT EXISTS users integer[] not null DEFAULT '{}'");
    }

    public function down()
    {
        Schema::dropIfExists('taraflex_katapulta_tasks');
    }
}
