<?php

namespace Taraflex\Katapulta\Models {

    use October\Rain\Database\Model;

/**
 * Task Model
 */
    class Task extends Model
    {
        use \October\Rain\Database\Traits\Validation;

        public $timestamps = false;

        public $attachMany = [];

        public $attachOne = [];

        public $belongsTo = [];

        public $belongsToMany = [];

        public $hasMany = [];

        /**
         * @var array Relations
         */
        public $hasOne = [];

        public $morphMany = [];

        public $morphOne = [];

        public $morphTo = [];

        /**
         * @var array Validation rules for attributes
         */
        public $rules = ['url' => 'required'];

        /**
         * @var string The database table used by the model.
         */
        public $table = 'taraflex_katapulta_tasks';

        /**
         * @var array Attributes to be appended to the API representation of the model (ex. toArray())
         */
        protected $appends = [];

        /**
         * @var array Attributes to be cast to native types
         */
        protected $casts = [];

        /**
         * @var array Attributes to be cast to Argon (Carbon) instances
         */
        protected $dates = [];

        /**
         * @var array Fillable fields
         */
        protected $fillable = [];

        /**
         * @var array Guarded fields
         */
        protected $guarded = ['*'];

        /**
         * @var array Attributes to be removed from the API representation of the model (ex. toArray())
         */
        protected $hidden = ['users'];

        /**
         * @var array Attributes to be cast to JSON
         */
        protected $jsonable = [];

        public function beforeCreate()
        {
            $this->socialType = $this->calcType();
        }

        public function beforeUpdate()
        {
            $this->socialType = $this->calcType();
        }

        protected function calcType()
        {
            switch (implode('.', array_slice(explode('.', parse_url($this->url, PHP_URL_HOST)), -2))) {
                case 'vk.com':
                    return 0;
                case 'fb.com':
                case 'fb.me':
                case 'facebook.com':
                    return 1;
                case 'youtu.be':
                case 'youtube.com':
                    return 2;
                case 'instagram.com':
                case 'instagr.am':
                    return 3;
            }
            throw new \Exception('Unsupported url type');
        }
    }

}
