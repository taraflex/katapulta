<?php

namespace Taraflex\Katapulta {

    use Backend\Facades\Backend;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Route;
    use System\Classes\PluginBase;
    use Taraflex\Katapulta\Models\Task;

    /**
     * katapulta Plugin Information File
     */
    class Plugin extends PluginBase
    {
        /**
         * Boot method, called right before the request route.
         *
         * @return array
         */
        public function boot()
        {
            foreach (['crx', 'zip'] as $ext) {
                Route::get('/download-extension-' . $ext, function () use ($ext) {
                    $workDir = realpath(__DIR__ . '/../../../');

                    $crx = realpath($workDir . '/extension_build/extension.' . $ext);
                    if (!file_exists($crx)) {
                        throw new \Exception("Run $workDir/genext.sh before");
                    }

                    $manifest = json_decode(file_get_contents($workDir . '/extension/manifest.json'));
                    return response()->download($crx, "{$manifest->short_name}.{$manifest->version}.$ext", [
                        'Cache-Control' => 'no-store, no-cache, max-age=0',
                        'Content-Type'  => 'application/octet-stream'
                    ]);
                });
            }

            //Дерьмовый внешний Api по историческим причинам - требовалось подогнать под уже существующее браузерное расширение

            Route::get('/get-tasks-{userId}', function ($userId) {
                $socialTypes = array_map('intval', array_filter(explode(',', $_GET['socTypes'] ?? ''), function ($v) {
                    return strlen($v) > 0;
                }));
                //так как eloquent не особо работает с array типом, воспользуемся сырым запросом
                return Task::whereIn('socialType', $socialTypes)->whereRaw('NOT (? = ANY (users))', [$userId])->limit(10)->get(['id', 'url', 'socialType']);
            });

            Route::get('/task-done-{taskId}', function ($taskId) {
                $taskId = intval($taskId);
                $userId = intval($_GET['vkid'] ?? 0);
                if ($userId && $taskId) {
                    //через eloquent не добавить элемент в postres array колонку - используем сырой запрос
                    Task::where('id', $taskId)->increment('processed', 1, [
                        'users' => DB::raw("array_append(users, $userId)") //$userId безопасно использовать в запросе после intval
                    ]);
                }
            });
        }

        /**
         * Returns information about this plugin.
         *
         * @return array
         */
        public function pluginDetails()
        {
            return [
                'name'        => 'katapulta',
                'description' => 'No description provided yet...',
                'author'      => 'taraflex',
                'icon'        => 'icon-leaf'
            ];
        }

        /**
         * Register method, called when the plugin is first registered.
         *
         * @return void
         */
        public function register()
        {

        }

        /**
         * Registers any front-end components implemented in this plugin.
         *
         * @return array
         */
        public function registerComponents()
        {
            return []; // Remove this line to activate
        }

        /**
         * Registers back-end navigation items for this plugin.
         *
         * @return array
         */
        public function registerNavigation()
        {
            return [
                'download_ext_crx' => [
                    'label'       => 'Download extension (crx)',
                    'url'         => '/download-extension-crx',
                    'icon'        => 'icon-download',
                    'permissions' => ['taraflex.katapulta.*'],
                    'order'       => 500
                ],
                'download_ext_zip' => [
                    'label'       => 'Download extension (zip)',
                    'url'         => '/download-extension-zip',
                    'icon'        => 'icon-download',
                    'permissions' => ['taraflex.katapulta.*'],
                    'order'       => 501
                ],
                'katapulta'        => [
                    'label'       => 'Katapulta',
                    'url'         => Backend::url('taraflex/katapulta/tasks'),
                    'icon'        => 'icon-link',
                    'permissions' => ['taraflex.katapulta.*'],
                    'order'       => 502
                ]
            ];
        }

        /**
         * Registers any back-end permissions used by this plugin.
         *
         * @return array
         */
        public function registerPermissions()
        {
            return []; // Remove this line to activate

            return [
                'taraflex.katapulta.some_permission' => [
                    'tab'   => 'katapulta',
                    'label' => 'Some permission'
                ]
            ];
        }
    }
}
